FROM node:16.13.2-alpine as builder

RUN mkdir -p /app

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build 

FROM nginx:1.21.5-alpine

COPY src/nginx/etc/conf.d/default.conf /etc/nginx/conf/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder app/dist/angular8-crud-demo usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]





